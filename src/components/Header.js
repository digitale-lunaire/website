import styles from "./Header.module.css";

function Header() {
  return (
    <header className={styles.navigation}>
      <div className={styles.container}>
        <a href="/" arira-label="Home" className={styles.title}>
          Digitale Lunaire
        </a>
        <nav>
          <a href="https://www.linkedin.com/in/luna-graysen-7461391a9/" className={styles.link}>
            LinkedIn
          </a>

          <a href="https://digitalelunaire.svbtle.com" className={styles.link}>
            Svbtle
          </a>
        </nav>
      </div>
    </header>
  );
}

export default Header;
