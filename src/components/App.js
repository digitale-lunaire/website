import Header from "./Header";
import Hero from "./Hero";
import Project from "./Project";
import Contact from "./Contact";
import Footer from "./Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Hero title="CI/CD expert and mobile engineer based in Montreal. Currently building Valora at cLabs." />
      <main>
        <Project
          title="English"
          content="Digitale Lunaire is a consulting firm specializing in building out new and optimizing existing continuous integration configurations for mobile first platforms. Luna Graysen has 6 years of mobile experience and 3 years of CI/CD experience working on the front lines at Bitrise, CircleCI and Travis CI. Digitale Lunaire is currently working on an exclusive contract with cLabs and is unavailable for other work."
        />
        <Project
          title="Français"
          content="Digitale Lunaire est une firme de conseil avec un spécialisation en développer et améliorer les configurations 'continuous integration' existantes pour les plate-formes mobile d'abord. Luna Graysen possède 6 ans d'expérience en mobile et 3 ans d'expérience CI/CD, travaillent en premier ligne pour Bitrise, CircleCI et Travis CI. Digitale Lunaire travaille au moment sur un contrat exclusif avec cLabs et ce soit pas disponible pour autres projets."
        />
        <Contact />
      </main>
      <Footer />
    </div>
  );
}

export default App;
