import styles from "./Footer.module.css";

function Footer() {
  return (
    <footer className={styles.container}>
      <div className={styles.content}>
        <div className={styles.contact}>
          <p>(438) 230-0188</p>
          <a href="mailto:info@digitalelunaire.ca">info@digitalelunaire.ca</a>
        </div>
        <div className={styles.copyright}>
          <p>Made with ♥︎ in Montreal</p>
        </div>
        <div className={styles.info}>
          <p>#173 3450 Rue Saint Denis</p>
          <p>Montreal, QC H2X 3L3</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
