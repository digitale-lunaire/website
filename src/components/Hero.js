import styles from "./Hero.module.css";

function Hero({ title }) {
  return (
    <section className={styles.container}>
      <div className={styles.content}>
        <h1>{title}</h1>
      </div>
    </section>
  );
}

export default Hero;
