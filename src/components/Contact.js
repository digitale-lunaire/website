import styles from "./Contact.module.css";

function Contact() {
  return (
    <section className={styles.container}>
      <a href="mailto:info@digitalelunaire.ca" className={styles.contact}>
        Get in Touch.
      </a>
    </section>
  );
}

export default Contact;
