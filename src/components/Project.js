import styles from "./Project.module.css";

function Project({ title, content }) {
  return (
    <section className={styles.container}>
      <div className={styles.content}>
        <div className={styles.separator} />
        <div></div>
        <div className={styles.info}>
          <p>{title}</p>
        </div>
        <div className={styles.text}>
          <p>{content}</p>
        </div>
      </div>
    </section>
  );
}

export default Project;
